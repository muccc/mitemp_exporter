{
  description = "Mitemp Exporter";

  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixos-23.05";
  inputs.flake-utils.url = "github:numtide/flake-utils";
  inputs.poetry2nix = {
    url = "github:nix-community/poetry2nix";
    inputs.nixpkgs.follows = "nixpkgs";
    inputs.flake-utils.follows = "flake-utils";
  };

  outputs = {
    self,
    nixpkgs,
    flake-utils,
    poetry2nix,
    ...
  }:
    {
      overlays.default = nixpkgs.lib.composeManyExtensions [
        poetry2nix.overlay
        (final: prev: {
          mitemp-exporter = final.poetry2nix.mkPoetryApplication {
            projectDir = final.poetry2nix.cleanPythonSources {src = ./.;};
            overrides = final.poetry2nix.overrides.withDefaults (self: super: {
              bluepy = super.bluepy.overridePythonAttrs (attrs: {
                buildInputs = attrs.buildInputs ++ [final.glib];
                nativeBuildInputs = attrs.nativeBuildInputs ++ [final.pkg-config];
              });
              btlewrap = super.btlewrap.overridePythonAttrs (attrs: {
                nativeBuildInputs = attrs.nativeBuildInputs ++ [self.setuptools];
              });
              mitemp-bt = super.mitemp-bt.overridePythonAttrs (attrs: {
                nativeBuildInputs = attrs.nativeBuildInputs ++ [self.setuptools];
              });
            });
          };
        })
      ];
    }
    // (flake-utils.lib.eachDefaultSystem (
      system: let
        pkgs = import nixpkgs {
          inherit system;
          overlays = [self.overlays.default];
        };
      in {
        formatter = pkgs.alejandra;

        packages = {
          default = pkgs.mitemp-exporter;
        };

        devShell = pkgs.mkShell {
          inputsFrom = [pkgs.mitemp-exporter];
          packages = with pkgs; [poetry glib pkg-config];
        };
      }
    ));
}
