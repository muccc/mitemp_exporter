from prometheus_client import start_http_server, Gauge
from mitemp_bt.mitemp_bt_poller import MiTempBtPoller, MI_TEMPERATURE, MI_HUMIDITY, MI_BATTERY
from btlewrap.bluepy import BluepyBackend
import click
import time

@click.command()
@click.option("--interval", default=120, help="poll interval in seconds")
@click.option("--port", default=9923, help="port to bind to")
@click.option("--addr", default="0.0.0.0", help="IP address to bind to")
@click.argument("macs", nargs=-1, required=True)
def main(interval, port, addr, macs):
  start_http_server(port, addr)

  last_polled = Gauge("mitemp_last_polled", "Unix Timestamp of last poll", ["mac"])
  temperature = Gauge("mitemp_temperature", "Temperature in C", ["mac"])
  humidity = Gauge("mitemp_humidity", "Relative Humidity", ["mac"])
  battery = Gauge("mitemp_battery", "Battery Level", ["mac"])

  devices = dict((mac, MiTempBtPoller(mac, BluepyBackend)) for mac in macs)

  while True:
      t1 = time.time()
      for mac, poller in devices.items():
          poller.fill_cache()
          last_polled.labels(mac).set_to_current_time()
          temperature.labels(mac).set(poller.parameter_value(MI_TEMPERATURE))
          humidity.labels(mac).set(poller.parameter_value(MI_HUMIDITY))
          battery.labels(mac).set(poller.parameter_value(MI_BATTERY))
      t2 = time.time()
      time.sleep(interval-(t2-t1))

if __name__ == '__main__':
    main()
